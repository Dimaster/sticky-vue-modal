# StickyModal

Now available for Vue.js.

## Usage

Install StickyModal for Vue.js through npm:

```
npm install sticky-modal-vue
```

## Browser Compatibility

StickyModal should work in most major browsers:

- Safari 9+ 
- Firefox 20+
- Chrome 20+
- Opera 15+
- Microsoft Edge

## Contribution

1. Fork the repository
2. Run `npm install`
3. Run `npm run dev` and start hacking. You can reach the example site at `http://localhost:8081`.
4. When you're done, run one final `npm run build` command and commit your work for a pull request.

### Guidelines
- tabs for indentation, 1 tab = 4 spaces
- camelCase method names
- _camelCase for private methods
- snake_case computed properties
- snake_case data
- kebab-case attributes
- arrow functions if possible
- be fully ES6 compliant!
